import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;

import org.json.JSONException;
import org.json.JSONObject;

public class JsonReader {
	
	//IMPORTANT : You will need external java library - http://maven.ibiblio.org/maven2/com/hp/hpl/jena/json-jena/1.0/json-jena-1.0.jar
	// Add this function to your class
	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	// Add this function to your class
	public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
		InputStream is = new URL(url).openStream();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			JSONObject json = new JSONObject(jsonText);
			return json;
		} finally {
			is.close();
		}
	}

	public static void main(String[] args) throws IOException, JSONException {
		//create a free account on bitbucket.org, then you can edit the jsob file below
		//copy the below line before you set power and clicks on each side
		JSONObject json = readJsonFromUrl("https://bitbucket.org/msahoo/rtix/raw/master/data.json");
		//change all reference as follows
		System.out.println("turnRightFrontTicks : " + json.getInt("turnRightFrontTicks"));
		System.out.println("turnLeftFrontTicks : " + json.getInt("turnLeftFrontTicks"));
	}
}
